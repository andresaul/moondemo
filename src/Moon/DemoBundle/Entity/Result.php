<?php

namespace Moon\DemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Result
 */
class Result
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $start_timestamp;

    /**
     * @var string
     */
    private $end_timestamp;

    /**
     * @var integer
     */
    private $type_id;

    /**
     * @var \DateTime
     */
    private $deleted_at;

    /**
     * @var \Moon\DemoBundle\Entity\Person
     */
    private $person;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set start_timestamp
     *
     * @param string $startTimestamp
     * @return Result
     */
    public function setStartTimestamp($startTimestamp)
    {
        $this->start_timestamp = $startTimestamp;

        return $this;
    }

    /**
     * Get start_timestamp
     *
     * @return string 
     */
    public function getStartTimestamp()
    {
        return $this->start_timestamp;
    }

    /**
     * Set end_timestamp
     *
     * @param string $endTimestamp
     * @return Result
     */
    public function setEndTimestamp($endTimestamp)
    {
        $this->end_timestamp = $endTimestamp;

        return $this;
    }

    /**
     * Get end_timestamp
     *
     * @return string 
     */
    public function getEndTimestamp()
    {
        return $this->end_timestamp;
    }

    /**
     * Set type_id
     *
     * @param integer $typeId
     * @return Result
     */
    public function setTypeId($typeId)
    {
        $this->type_id = $typeId;

        return $this;
    }

    /**
     * Get type_id
     *
     * @return integer 
     */
    public function getTypeId()
    {
        return $this->type_id;
    }

    /**
     * Set deleted_at
     *
     * @param \DateTime $deletedAt
     * @return Result
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deleted_at = $deletedAt;

        return $this;
    }

    /**
     * Get deleted_at
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * Set person
     *
     * @param \Moon\DemoBundle\Entity\Person $person
     * @return Result
     */
    public function setPerson(\Moon\DemoBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \Moon\DemoBundle\Entity\Person 
     */
    public function getPerson()
    {
        return $this->person;
    }
}
