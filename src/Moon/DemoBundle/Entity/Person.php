<?php

namespace Moon\DemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Person
 */
class Person
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $number;

    /**
     * @var integer
     */
    private $chipId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Person
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Person
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set chipId
     *
     * @param integer $chipId
     * @return Person
     */
    public function setChipId($chipId)
    {
        $this->chipId = $chipId;

        return $this;
    }

    /**
     * Get chipId
     *
     * @return integer 
     */
    public function getChipId()
    {
        return $this->chipId;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $reults;


    /**
     * Add reults
     *
     * @param \Moon\DemoBundle\Entity\Result $reults
     * @return Person
     */
    public function addReult(\Moon\DemoBundle\Entity\Result $reults)
    {
        $this->reults[] = $reults;

        return $this;
    }

    /**
     * Remove reults
     *
     * @param \Moon\DemoBundle\Entity\Result $reults
     */
    public function removeReult(\Moon\DemoBundle\Entity\Result $reults)
    {
        $this->reults->removeElement($reults);
    }

    /**
     * Get reults
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReults()
    {
        return $this->reults;
    }
}
