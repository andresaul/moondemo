<?php

namespace Moon\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ResultController extends Controller
{

    public function indexAction()
    {
        return $this->render('MoonDemoBundle:Moon:index.html.php');
    }

}