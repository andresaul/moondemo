<?php

namespace Moon\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Moon\DemoBundle\Entity\Result;
use Moon\DemoBundle\Entity\Person;

class ApiController extends Controller
{

    /**
     *
     * This is Api for handling finish control points data. This API uses a JSON format.
     *
     * @ApiDoc(
     *  resource=false,
     *  description="",
     *  parameters={
     *      {"name"="chip_id", "dataType"="integer", "required"=true, "description"="chip_id"},
     *      {"name"="type_id", "dataType"="integer", "required"=true, "description"="finish type_id"},
	 *		{"name"="endtime", "dataType"="datetime", "required"=true, "description"="Finishing time", "format"="Y-m-d H:i:s.u"},
     *  }
     * )
     */

    public function indexAction(Request $request)
    {
		$requestToken = $request->query->get('access_token');
		$apiService = $this->get('api_service');
		if(false === $apiService->isApiTokenValid($requestToken) )
			return new JsonResponse( array('data' => 'Token mismatch!'), 401); 

		$requestContent = $request->getContent();
		$deserializedArray = $apiService->deserializedRequestArray($requestContent);

		$time = new Result();	
		$time->setTypeId($deserializedArray['type_id']);

		if(2 === (int)$deserializedArray['type_id']) {

			$d2 = explode('.', $deserializedArray["endtime"]);
			$d1 = strtotime($d2['0'])  . '.' . $d2['1'];

			$time->setEndTimestamp($d1 );
		}

		$competition_start_time =  $this->container->getParameter('competition_start_time');
		$startTimestamp = strtotime($competition_start_time);
		$time->setStartTimestamp($startTimestamp);


		$em = $this->getDoctrine()->getManager();

		$person = $em->getRepository('MoonDemoBundle:Person')->findOneBy(array('chipId' => $deserializedArray['chip_id']) );
		$time->setPerson($person);
		
   		$em->persist($time);

		$dublicate = $em->getRepository('MoonDemoBundle:Result')->findActiveByChipId($deserializedArray['chip_id']);
		if($dublicate) {
			$dublicate->setDeletedAt(new \DateTime(date('Y-m-d H:i:s')));
		}

    	$em->flush();


		$pusher = $this->container->get('lopi_pusher.pusher');
		$pusherResponse = $pusher->trigger( 'my-channel', 'my_event', '$respose2' );

        $response = new JsonResponse(array('success' => 1, 'pusher_response' => $pusherResponse));
        return $response;
    }



    public function resultJsonAction() {

		$em = $this->getDoctrine()->getManager();
		$resultLimit = $this->container->getParameter('result_limit');
		$results = $em->getRepository('MoonDemoBundle:Result')->getResultList($resultLimit);
		
		foreach($results as $key => $time) {

			$results[$key]['end_timestamp'] = $results[$key]['end_timestamp'] * 1000;
			$results[$key]['start_timestamp'] = $results[$key]['start_timestamp'] * 1000;
			$diff = $results[$key]['end_timestamp'] - $results[$key]['start_timestamp'];

			if($time['type_id'] == 1) {

				$results[$key]['diff_formated'] = null;
			} else {
				
				$results[$key]['diff_formated'] = $this->formatMilliseconds($diff/1000) ;
			}
		}

    	return new JsonResponse( $results);
    }


    private function formatMilliseconds($difference) {

		$second = 1;
		$minute = 60*$second;
		$hour   = 60*$minute;
		$day    = 24*$hour;

	
		$ans["day"]    = floor($difference/$day);
		$ans["hour"]   = floor(($difference%$day)/$hour);
		$ans["minute"] = floor((($difference%$day)%$hour)/$minute);
		$ans["second"] = floor(((($difference%$day)%$hour)%$minute)/$second);

		$msec = number_format(round($difference, 3), 3);
		$floatpart = explode('.', $msec );
		$ans["msecond"] = $floatpart['1'];

		return $ans["day"] . " days, " . $ans["hour"] . " hours, "  . $ans["minute"] . " minutes, " . $ans["second"] . " seconds  " . $ans["msecond"] . " mseconds";
	
	}


}

