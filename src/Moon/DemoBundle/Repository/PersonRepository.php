<?php
namespace Moon\DemoBundle\Repository;
use Doctrine\ORM\EntityRepository;

class PersonRepository extends EntityRepository
{
  
    public function findActiveByChipId($chip_id) {

         $qb = $this->createQueryBuilder('p')
                   ->select('p')
                   ->addSelect('ft')
                   //->where('.deleted_at IS NOT NULL')
                   ->leftJoin('p.results', 'ft')

                   ->andWhere('p.chipId = :chip_id')
                   ->setParameter('chip_id',  $chip_id) 
                   ->setMaxResults(1) 
                    ;

        $query = $qb->getQuery();

        return $query->getArrayResult();
    }

   public function findOneByChipId($chip_id) {

         $qb = $this->createQueryBuilder('p')

                   ->where('p.chipId = :chip_id')
                   ->setParameter('chip_id',  $chip_id) 
                   ->setMaxResults(1) 
                    ;

        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }

}