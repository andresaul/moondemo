<?php
namespace Moon\DemoBundle\Repository;
use Doctrine\ORM\EntityRepository;

class ResultRepository extends EntityRepository
{

    public function getResultList($limit = 100) {


         $qb = $this->createQueryBuilder('ft')
                   ->select('ft.id')
                   ->addSelect('ft.type_id')

                   ->addSelect('p.name')
                   ->addSelect('p.number AS person_number')
                   ->addSelect('p.chipId AS chip_id')
                   ->addSelect('ft.start_timestamp')
                   ->addSelect('ft.end_timestamp')

                   ->leftJoin('ft.person', 'p')

                   ->where('ft.deleted_at IS NULL')
                   
                   ->addOrderBy('ft.end_timestamp', 'ASC')
                   ->addOrderBy('ft.id', 'DESC')
                   ->setMaxResults($limit)
                ;


        $query = $qb->getQuery();

        return $query->getArrayResult();

    }

    public function findActiveByChipId($chip_id) {


         $qb = $this->createQueryBuilder('ft')
                   ->select('ft')
                   ->where('ft.deleted_at IS NULL')
                   ->innerJoin('ft.person', 'p')

                   ->andWhere('p.chipId = :chip_id')
                   ->setParameter('chip_id',  $chip_id) 
                   ->setMaxResults(1) 

                   ->orderBy('ft.id', 'DESC')
                    ;

        $query = $qb->getQuery();
            //var_dump($query);
       // return $query->getArrayResult();
        return $query->getOneOrNullResult();
    }
}
