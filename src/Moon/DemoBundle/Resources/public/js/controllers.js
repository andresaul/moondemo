
var resusltListApp = angular.module('resusltListApp', ['pusher-angular']);

resusltListApp.controller('ResultListCtrl', ['$pusher', '$scope', '$http', function($pusher, $scope, $http) {
 
    // Exceuted on first load
    angular.element(document).ready(function () {
        
        $http.get('http://80.240.135.190/app.php/result_list.json').success(function(data) {
            $scope.results = data;
        });        
    });

    // Initialise our Pusher
    var client = new Pusher('16f8b1972a4536fb1a32');
    var pusher = $pusher(client);
 
    // Subscribe to the test channel
    var my_channel = pusher.subscribe('my-channel');
    
    my_channel.bind('my_event', function(data) {
        // console.log('Update recvd:' + data);

        $http.get('http://80.240.135.190/app.php/result_list.json').success(function(data) {
            $scope.results = data;
        });
    });

  }]);