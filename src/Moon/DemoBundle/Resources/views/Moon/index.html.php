<?php $view->extend('MoonDemoBundle::base.html.php') ?>

<?php $view['slots']->set('title', 'My cool blog posts') ?>
<?php $view['slots']->start('body') ?>



	<table id="resultTable" class="table table-hover">
       <th>Person name</th>
       <th>Person number</th>
   <!--    <th>id</th>
       <th>Type_id</th> -->
       <th>Chip_id</th>
       <th>End time</th>
       <th>Duration</th>

       <tr ng-repeat="result in results">
            <td>{{result.name}}</td>
            <td>{{result.person_number}}</td>
            <td>{{result.chip_id}} </td>       
          <!--   <td>{{result.type_id}} </td>       
            <td>{{result.id}}</td> -->
            <td>{{result.end_timestamp ? (result.end_timestamp | date:'yyyy-MM-dd HH:mm:ss.sss'):'-' }}</td>

            <td>{{result.diff_formated}}</td>
       </tr>     
	</table>

<?php $view['slots']->stop() ?> 