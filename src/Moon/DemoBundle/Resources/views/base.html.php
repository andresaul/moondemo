<!DOCTYPE html>
<html lang="en" ng-app="resusltListApp">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!-- CSS -->
		<link rel="stylesheet" href="/bundles/pinanobootstrap3/css/bootstrap.css" />

		<!-- JS -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script src="/bundles/pinanobootstrap3/js/bootstrap.js"></script>

		<!-- AngularJS -->
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0/angular.min.js"></script>

		<!-- pusher-js -->
		<script src="//js.pusher.com/2.2/pusher.min.js"></script>

		<!-- pusher-angular -->
		<script src="//cdn.jsdelivr.net/angular.pusher/latest/pusher-angular.min.js"></script>

		<!-- Angular ResultController-->
		<script src="/bundles/moondemo/js/controllers.js"></script>
		
        <title><?php $view['slots']->output('title', 'Moon Demo Application') ?></title>
    </head>
    
    <body ng-controller="ResultListCtrl">
	<div class="container">
		<div class="col-md-12 column">
			<nav class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
				 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					  <span class="sr-only">Toggle navigation</span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span><span class="icon-bar"></span>
				 </button> 
				 <a class="navbar-brand" href="#">Moon Demo App</a>
			</div>
			
			<ul class="nav navbar-nav" style="float:right">
				<li class="active">
					<span>Competition start: <?php echo $view->container->getParameter('competition_start_time') ?></span>
				</li>
			</ul>
			<!--
           <ul class="nav navbar-nav">
                    <li class="">
                    <a href="#">Link2</a>
                    </li>
            </ul> -->
		</nav>
		</div>
			<div class="row clearfix">
				<div class="col-md-12 column">

					<?php $view['slots']->output('body') ?>

				</div>
			</div>
	</div>
    </body>
</html>
