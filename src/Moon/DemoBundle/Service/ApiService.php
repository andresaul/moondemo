<?php
 
namespace Moon\DemoBundle\Service;

#use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;
//use LoopIt\CrmBundle\Entity\ProjectPriceOfferArticle;
//use LoopIt\CrmBundle\Entity\ProjectPriceOfferHasArticle;
//use LoopIt\CrmBundle\Form\ProjectPriceOfferHasArticleType;


class ApiService
{

	   /**
     * service container
     *
     * @var object
     */
    protected $service;
 
    /**
     * init service
     *
     * @param object $service
     * @return $this
     */

    public function __construct($service) {

        $this->service = $service;
 
        return $this;
    }

	public function isApiTokenValid($requestToken) {

		$token = $this->service->getParameter('api_token');
        if($requestToken === $token) {
            
            return true;
        } else {
            return false;
        }

	}

    public function deserializedRequestArray($requestContent ) {
        
        $deserializedArray = null;

        if($requestContent) {
            $format = 'json';
            $typeName = 'array';
            $serializer = $this->service->get('jms_serializer');       
            $deserializedArray = $serializer->deserialize($requestContent, $typeName, $format);      
        }
       // var_dump($deserializedArray);
        return $deserializedArray;
    }

}