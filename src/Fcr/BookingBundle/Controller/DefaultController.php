<?php

namespace Fcr\BookingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('FcrBookingBundle:Default:index.html.twig', array('name' => $name));
    }
}
